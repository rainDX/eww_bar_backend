# eww bar backend

A eww backend with multiple modules :
  - hyperland state (workspaces, modules, etc...)

## Installation

```sh
cargo install --path . --root ~/.config/eww/
```

## Usage
In eww config :
```
(deflisten backend "bin/eww_bar_backend")
;; ....
;; for the hyprland workspaces
(defwidget workspaces []
  (eventbox :onscroll "~/.config/eww/scripts/change-active-workspace {} ${backend.hyprland.active_workspace}" :class "workspaces"
    (box :space-evenly false :orientation "h"
      (for workspace in "${backend.hyprland.workspaces}"
          (box :class "workspace-entry ${workspace.current ? "current" : ""} ${workspace.occupied ? "occupied" : "empty"} ${workspace.urgent ? "urgent" : ""}"
            (button :onclick "hyprctl dispatch workspace ${workspace.id}" "${workspace.id}")
          )))))

;; current window title
(defwidget window []
  (box
    :class "module"
    :space-evenly false
    :orientation "h"
      (label
        :class "window"
        :limit-width 42
        :text "${backend.hyprland.active_window}"
        :tooltip "${backend.hyprland.active_window}")))

```
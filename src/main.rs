/*
* SPDX-FileCopyrightText: Copyright © 2023 Romain Dijoux
* SPDX-License-Identifier: MIT
*/

pub mod modules;

use serde_json::json;
use std::{io::Write, sync::mpsc, collections::HashMap};
use xdg::BaseDirectories;

use config::Config;
use modules::{MessageProducer, ThreadMessage};

#[cfg(feature = "hyprland")]
use modules::hyprland_monitor::HyprlandMonitoring;
#[cfg(feature = "sensors")]
use modules::sensors_monitor::SensorMonitoring;
#[cfg(feature = "nm")]
use modules::network_monitor::NetworkMonitoring;
#[cfg(feature = "upower")]
use modules::upower_monitor::UPowerMonitoring;

fn main() {
    let base_dirs = BaseDirectories::with_prefix("eww").unwrap();
    let config_path = base_dirs.place_config_file("backend_config.toml").unwrap();
    if !config_path.exists() {
        let mut file = std::fs::File::create(&config_path).unwrap();
        file.write_all(b"# Configuration file\n").unwrap();
    }

    let config = Config::builder()
        .add_source(config::File::with_name(&config_path.to_str().unwrap()))
        .build()
        .unwrap();

    let (sender, receiver) = mpsc::channel();

    #[cfg(feature = "hyprland")]
    {
        let hyprland_monitor = HyprlandMonitoring::new(sender.clone(), &config);
        hyprland_monitor.run();
    }
    #[cfg(feature = "sensors")]
    {
        let sensor_monitor = SensorMonitoring::new(sender.clone(), &config);
        sensor_monitor.run();
    }
    #[cfg(feature = "nm")]
    {
        let net_monitor = NetworkMonitoring::new(sender.clone(), &config);
        net_monitor.run();
    }
    #[cfg(feature = "upower")]
    {
        let upower_monitor = UPowerMonitoring::new(sender.clone(), &config);
        upower_monitor.run();
    }

    let mut output = HashMap::new();

    loop {
        match receiver.recv() {
            Ok(message) => match message {
                ThreadMessage::JsonValue(module, val) => {
                    output.insert(module, val);
                    println!("{}", json!(output));
                },
                ThreadMessage::Terminate => {}
            },
            Err(_) => {
                break; // Break the loop if the channel is closed
            }
        }
    }
}

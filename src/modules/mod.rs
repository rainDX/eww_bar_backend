/*
* SPDX-FileCopyrightText: Copyright © 2023 Romain Dijoux
* SPDX-License-Identifier: MIT
*/

#[cfg(feature = "hyprland")]
pub mod hyprland_monitor;

#[cfg(feature = "sensors")]
pub mod sensors_monitor;

#[cfg(feature = "nm")]
pub mod network_monitor;

#[cfg(feature = "upower")]
pub mod upower_monitor;

use std::sync::mpsc;

use config::Config;
pub enum ThreadMessage {
    JsonValue(String, serde_json::Value),
    Terminate,
}
pub trait MessageProducer {
    fn new(sender: mpsc::Sender<ThreadMessage>, config: &Config) -> Self;
    fn run(&self);
}

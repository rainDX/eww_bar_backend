/*
* SPDX-FileCopyrightText: Copyright © 2023 Romain Dijoux
* SPDX-License-Identifier: MIT
*/

use std::sync::mpsc;
use std::thread;

use config::Config;
use serde::Serialize;
use serde_json::json;

use crate::modules::{MessageProducer, ThreadMessage};

use hyprland::data::{Client, Clients, Monitors, Workspaces};
use hyprland::event_listener::EventListenerMutable;
use hyprland::prelude::*;
use hyprland::shared::{HyprData, HyprDataVec};

#[derive(Serialize)]
struct HyprlandWorkspace {
    pub id: i32,
    pub name: String,
    pub current: bool,
    pub occupied: bool,
    pub urgent: bool,
}

#[derive(Serialize)]
struct HyprlandState {
    pub workspaces: Vec<HyprlandWorkspace>,
    pub active_monitor: i16,
    pub active_workspace: i32,
    pub active_window: String,
}

pub struct HyprlandMonitoring {
    sender: mpsc::Sender<ThreadMessage>,
    workspaces: Option<Vec<i32>>,
    hide_specials: bool,
}

fn set_urgent_state(urgent_address: String, state: &mut HyprlandState) {
    let clients = Clients::get().expect("unable to get clients").to_vec();
    match clients.iter().find(|c| {
        eprintln!("{}", c.address.to_string());
        urgent_address == c.address.to_string()
    }) {
        Some(client) => {
            match state
                .workspaces
                .iter_mut()
                .find(|w| w.id == client.workspace.id)
            {
                Some(workspace) => workspace.urgent = true,
                None => {
                    eprintln!("unable to find URGENT client workspace");
                }
            }
        }
        None => {
            eprintln!("unable to find URGENT client {}", urgent_address);
        }
    };
}

fn update_state(workspace_ids: &Option<Vec<i32>>, hide_specials: bool) -> HyprlandState {
    let mut workspaces = Workspaces::get()
        .expect("unable to get workspaces")
        .to_vec();
    workspaces.sort_by_key(|w| w.id);

    if hide_specials {
        workspaces = workspaces.into_iter().filter(|w| w.id >= 0).collect();
    }
    
    let active_monitor = Monitors::get()
        .expect("unable to get monitors")
        .find(|m| m.focused == true)
        .unwrap();

    let active_window = match Client::get_active().expect("Failed to get active window") {
        Some(window) => window.title,
        None => "".to_owned(),
    };

    let active_monitor_id = active_monitor.id;
    let active_workspace = active_monitor.active_workspace.id;

    let mut hyprland_workspaces = Vec::new();

    match workspace_ids {
        Some(ids) => {
            let mut ids = ids.clone();
            ids.sort_by(|a, b| b.cmp(a));
            for w in &workspaces {
                while !ids.is_empty() && *ids.last().unwrap() < w.id {
                        hyprland_workspaces.push(HyprlandWorkspace {
                            id: *ids.last().unwrap(),
                            name: ids.last().unwrap().to_string(),
                            current: false,
                            occupied: false,
                            urgent: false,
                        });
                    ids.pop();
                }
                if !ids.is_empty() && *ids.last().unwrap() == w.id {
                    ids.pop();
                }
                hyprland_workspaces.push(HyprlandWorkspace {
                    id: w.id,
                    name: w.name.clone(),
                    current: w.id == active_workspace,
                    occupied: w.windows > 0,
                    urgent: false,
                });
            }
            while !ids.is_empty() {
                hyprland_workspaces.push(HyprlandWorkspace {
                    id: *ids.last().unwrap(),
                    name: ids.last().unwrap().to_string(),
                    current: false,
                    occupied: false,
                    urgent: false,
                });
                ids.pop();
            }
        }
        None => {
            for w in &workspaces {
                hyprland_workspaces.push(HyprlandWorkspace {
                    id: w.id,
                    name: w.name.clone(),
                    current: w.id == active_workspace,
                    occupied: w.windows > 0,
                    urgent: false,
                });
            }
        }
    }

    HyprlandState {
        workspaces: hyprland_workspaces,
        active_monitor: active_monitor_id,
        active_window,
        active_workspace,
    }
}

fn monitor(
    sender: mpsc::Sender<ThreadMessage>,
    workspace_ids: Option<Vec<i32>>,
    hide_specials: bool,
) {
    let module_name = "hyprland";
    let mut event_listener = EventListenerMutable::new();
    let state = update_state(&workspace_ids, hide_specials);
    sender
        .send(ThreadMessage::JsonValue(
            module_name.to_owned(),
            json!(state),
        ))
        .unwrap();

    {
        let sender = sender.clone();
        let workspace_ids = workspace_ids.clone();
        event_listener.add_urgent_state_handler(move |data, _| {
            let mut state = update_state(&workspace_ids, hide_specials);
            set_urgent_state("0x".to_owned() + &data.to_string(), &mut state);
            sender
                .send(ThreadMessage::JsonValue(
                    module_name.to_owned(),
                    json!(state),
                ))
                .unwrap();
        });
    }

    macro_rules! add_handler {
        ($listener:expr, $handler:expr) => {
            let sender = sender.clone();
            let workspace_ids = workspace_ids.clone();
            $handler(&mut $listener, move |_, _| {
                let state = update_state(&workspace_ids, hide_specials);
                sender
                    .send(ThreadMessage::JsonValue(
                        module_name.to_owned(),
                        json!(state),
                    ))
                    .unwrap();
            });
        };
    }
    add_handler!(
        event_listener,
        EventListenerMutable::add_workspace_change_handler
    );
    add_handler!(
        event_listener,
        EventListenerMutable::add_workspace_added_handler
    );
    add_handler!(
        event_listener,
        EventListenerMutable::add_workspace_destroy_handler
    );
    add_handler!(
        event_listener,
        EventListenerMutable::add_workspace_moved_handler
    );
    add_handler!(
        event_listener,
        EventListenerMutable::add_active_monitor_change_handler
    );
    add_handler!(
        event_listener,
        EventListenerMutable::add_active_window_change_handler
    );
    add_handler!(
        event_listener,
        EventListenerMutable::add_monitor_added_handler
    );
    add_handler!(
        event_listener,
        EventListenerMutable::add_monitor_removed_handler
    );
    add_handler!(
        event_listener,
        EventListenerMutable::add_window_open_handler
    );
    add_handler!(
        event_listener,
        EventListenerMutable::add_window_close_handler
    );
    add_handler!(
        event_listener,
        EventListenerMutable::add_window_moved_handler
    );

    event_listener.start_listener().unwrap();
}

impl MessageProducer for HyprlandMonitoring {
    fn new(sender: mpsc::Sender<ThreadMessage>, config: &Config) -> Self {
        let workspaces: Option<Vec<i32>> = match config.get_array("hyprland.workspaces") {
            Ok(ws) => Some(
                ws.iter()
                    .map(|value| {
                        value
                            .clone()
                            .into_int()
                            .expect("Failed to read workspace number in config")
                            .try_into()
                            .expect("workspace id not a 32bit integer")
                    })
                    .collect::<Vec<i32>>(),
            ),
            Err(_) => None,
        };
        let hide_specials = config.get_bool("hyprland.hide_specials").unwrap_or(false);
        HyprlandMonitoring {
            sender,
            workspaces,
            hide_specials,
        }
    }

    fn run(&self) {
        let sender = self.sender.clone();
        let workspaces = self.workspaces.clone();
        let hide_specials = self.hide_specials;
        thread::spawn(move || {
            monitor(sender, workspaces, hide_specials);
        });
    }
}

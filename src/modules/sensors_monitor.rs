/*
* SPDX-FileCopyrightText: Copyright © 2023 Romain Dijoux
* SPDX-License-Identifier: MIT
*/

use std::sync::mpsc;
use std::thread;
use std::time;

use config::Config;
use serde::Serialize;
use serde_json::json;

use crate::modules::{MessageProducer, ThreadMessage};

use libmedium::{
    hwmon::Hwmon,
    parse_hwmons,
    sensors::{temp::TempSensor, Sensor},
};

/// Returns the index of the hwmon coretemp
/// if it is not found it falls back to acpitz and then to the first
///
/// # Arguments
///
/// * `hwmons` - A Hwmon vector
///
fn get_main_hwmon_idx(hwmons: &Vec<Hwmon>) -> u16 {
    debug_assert!(hwmons.len() > 0);
    let (mut index, mut i): (usize, usize) = (0, 0);
    let mut coretemp_found = false;
    while !coretemp_found && i < hwmons.len() {
        match hwmons[i].name() {
            "coretemp" => {
                index = i;
                coretemp_found = true;
            }
            "k10temp" => {
                index = i;
                coretemp_found = true;
            }
            "acpitz" => index = i,
            &_ => {}
        }
        i += 1;
    }
    hwmons[index].index()
}

/// Returns a vector containing all the temperature-related hwmons
fn get_temp_hwmons() -> Vec<Hwmon> {
    let mut temp_hwmons = Vec::new();
    (&parse_hwmons().unwrap()).into_iter().for_each(|hwmon| {
        if hwmon.temps().len() > 0 {
            temp_hwmons.push(hwmon.to_owned());
        }
    });
    temp_hwmons
}

#[derive(Serialize)]
struct SensorState {
    pub name: String,
    pub current: f64,
    pub max: f64,
}

#[derive(Serialize)]
struct MonitorState {
    pub name: String,
    pub sensors: Vec<SensorState>,
}

#[derive(Serialize)]
struct State {
    pub main: f64,
    pub max_main: f64,
    pub monitors: Vec<MonitorState>,
}

pub struct SensorMonitoring {
    sender: mpsc::Sender<ThreadMessage>,
    polling_ms: u64,
}

impl MessageProducer for SensorMonitoring {
    fn new(sender: mpsc::Sender<ThreadMessage>, config: &Config) -> Self {
        let polling_ms = match config.get_int("sensors.polling_ms") {
            Ok(ms) => ms as u64,
            Err(_) => 1000,
        };
        SensorMonitoring { sender, polling_ms }
    }

    fn run(&self) {
        let sender = self.sender.clone();
        let polling_ms = self.polling_ms;
        thread::spawn(move || {
            let hwmons = get_temp_hwmons();
            loop {
                let mut state = State {
                    main: f64::MIN,
                    max_main: 100.0,
                    monitors: Vec::new(),
                };
                let main_idx = get_main_hwmon_idx(&hwmons);
                for hwmon in &hwmons {
                    if hwmon.index() == main_idx {
                        for (_, temp_sensor) in hwmon.temps() {
                            let input = match temp_sensor.read_input() {
                                Ok(temp) => temp.as_degrees_celsius(),
                                Err(_) => 0.0_f64,
                            };
                            if input > state.main {
                                state.main = input;
                                match temp_sensor.read_crit() {
                                    Ok(temp) => state.max_main = temp.as_degrees_celsius(),
                                    Err(_) => {}
                                };
                            }
                        }
                    }
                    let mut monitor = MonitorState {
                        name: hwmon.name().to_owned(),
                        sensors: Vec::new(),
                    };
                    for (_, temp_sensor) in hwmon.temps() {
                        let current = match temp_sensor.read_input() {
                            Ok(temp) => temp.as_degrees_celsius(),
                            Err(_) => 0.0_f64,
                        };
                        let max = match temp_sensor.read_crit() {
                            Ok(temp) => temp.as_degrees_celsius(),
                            Err(_) => 100.0_f64,
                        };
                        monitor.sensors.push(SensorState {
                            name: temp_sensor.name(),
                            current,
                            max,
                        });
                    }
                    state.monitors.push(monitor);
                }
                sender
                    .send(ThreadMessage::JsonValue("sensors".to_owned(), json!(state)))
                    .unwrap();
                thread::sleep(time::Duration::from_millis(polling_ms));
            }
        });
    }
}

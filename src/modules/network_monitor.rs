/*
* SPDX-FileCopyrightText: Copyright © 2023 Romain Dijoux
* SPDX-License-Identifier: MIT
*/

use std::sync::mpsc;
use std::thread;

use config::Config;

use serde::Serialize;
use serde_json::json;

use crate::modules::{MessageProducer, ThreadMessage};

use async_std::task;
use cosmic_dbus_networkmanager::active_connection::ActiveConnection;
use cosmic_dbus_networkmanager::interface::access_point::AccessPointProxy;
use cosmic_dbus_networkmanager::interface::device::wired::WiredDeviceProxy;
use cosmic_dbus_networkmanager::interface::device::DeviceProxy;
use cosmic_dbus_networkmanager::interface::enums::NmState;
use cosmic_dbus_networkmanager::nm;
// use futures::stream::StreamExt;
use zbus::Connection;

#[derive(Serialize)]
enum ConnectionState {
    Disconnected,
    Connecting,
    Limited,
    Connected,
}

#[derive(Serialize)]
struct NetworkState {
    pub state: ConnectionState,
    pub wireless: bool,
    pub ssid: String,
    pub ifname: String,
    pub ip4_addr: String,
    pub ip6_addr: String,
    pub signal_strength: u8,
    pub frequency: u32,
    pub max_speed: u32,
}

impl NetworkState {
    fn new() -> Self {
        NetworkState {
            state: ConnectionState::Disconnected,
            wireless: false,
            ssid: String::new(),
            ifname: String::new(),
            ip4_addr: String::new(),
            ip6_addr: String::new(),
            signal_strength: 0,
            frequency: 0,
            max_speed: 0,
        }
    }
}

pub struct NetworkMonitoring {
    sender: mpsc::Sender<ThreadMessage>,
    polling_s: u64,
}

macro_rules! get_zbus_property {
    ($method:expr) => {
        match $method.await {
            Ok(prop) => Some(prop),
            Err(_) => None,
        }
    };
}

macro_rules! get_zbus_property_fallback {
    ($method:expr,$fallback:expr) => {
        match $method.await {
            Ok(prop) => prop,
            Err(_) => $fallback,
        }
    };
}

macro_rules! extract_address_string {
    ($ipcfg:expr) => {
        match $ipcfg {
            Some(ipcfg) => {
                let addresses = get_zbus_property!(ipcfg.address_data());
                match addresses {
                    Some(addresses) => {
                        if addresses.len() > 0 {
                            addresses[0].address.to_string()
                        } else {
                            "?".to_string()
                        }
                    }
                    None => "?".to_owned(),
                }
            }
            None => "?".to_owned(),
        }
    };
}

async fn get_access_point<'a>(
    nm: &'a nm::NetworkManager<'a>,
    active: &'a ActiveConnection<'a>,
) -> Option<AccessPointProxy<'a>> {
    let path = get_zbus_property!(active.specific_object());
    match path {
        Some(path) => match AccessPointProxy::builder(nm.connection()).path(path) {
            Ok(builder) => {
                let access_point = builder.build().await;
                match access_point {
                    Ok(access_point) => Some(access_point),
                    Err(_) => None,
                }
            }
            Err(_) => None,
        },
        None => None,
    }
}

async fn get_wired<'a>(
    nm: &'a nm::NetworkManager<'a>,
    device: &'a DeviceProxy<'a>,
) -> Option<WiredDeviceProxy<'a>> {
    let path = device.path();
    match WiredDeviceProxy::builder(nm.connection()).path(path) {
        Ok(builder) => match builder.build().await {
            Ok(wired) => Some(wired),
            Err(_) => None,
        },
        Err(_) => None,
    }
}

async fn get_active_connection_info(nm: &nm::NetworkManager<'_>) -> NetworkState {
    let mut netstate = NetworkState::new();
    let nm_state = nm.state().await.expect("Failed to query network state");

    match nm_state {
        NmState::Connecting => {
            netstate.state = ConnectionState::Connecting;
        }
        NmState::ConnectedLocal | NmState::ConnectedSite => {
            netstate.state = ConnectionState::Limited;
        }
        NmState::ConnectedGlobal => {
            netstate.state = ConnectionState::Connected;
        }
        _ => {}
    };

    if matches!(
        netstate.state,
        ConnectionState::Limited | ConnectionState::Connected
    ) {
        let actives = nm
            .active_connections()
            .await
            .expect("Failed to get active connections");

        if actives.len() > 0 {
            let current = &actives[0];
            let network_type = get_zbus_property_fallback!(current.type_(), "".to_owned());
            let ip4cfg = get_zbus_property!(current.ip4_config());
            let ip6cfg = get_zbus_property!(current.ip6_config());

            netstate.ip4_addr = extract_address_string!(ip4cfg);
            netstate.ip6_addr = extract_address_string!(ip6cfg);

            if network_type == "802-11-wireless" {
                netstate.wireless = true;
                let access_point = get_access_point(nm, current).await;
                match access_point {
                    Some(access_point) => {
                        netstate.ssid = String::from_utf8(get_zbus_property_fallback!(
                            access_point.ssid(),
                            vec![]
                        ))
                        .unwrap_or("".to_owned());
                        netstate.frequency =
                            get_zbus_property_fallback!(access_point.frequency(), 0);
                        netstate.signal_strength =
                            get_zbus_property_fallback!(access_point.strength(), 0);
                        netstate.max_speed =
                            get_zbus_property_fallback!(access_point.max_bitrate(), 0) / 1000;
                    }
                    None => {}
                }
            }
            let device = {
                match current.devices().await {
                    Ok(devices) => {
                        if devices.len() > 0 {
                            Some(devices[0].clone())
                        } else {
                            None
                        }
                    }
                    Err(_) => None,
                }
            };

            match device {
                Some(device) => {
                    netstate.ifname =
                        get_zbus_property_fallback!(device.interface(), "".to_owned());
                    if network_type == "802-3-ethernet" {
                        match get_wired(&nm, &device).await {
                            Some(wired) => {
                                netstate.max_speed = get_zbus_property_fallback!(wired.speed(), 0);
                            }
                            None => {}
                        }
                    }
                }
                None => {}
            }

            match current.get_property::<String>("Ssid").await {
                Ok(ssid) => netstate.ssid = ssid,
                Err(_) => {}
            }
        } else {
            netstate.state = ConnectionState::Disconnected;
        }
    }
    netstate
}

async fn monitor(sender: mpsc::Sender<ThreadMessage>, polling_s: u64) {
    let connection = Connection::system()
        .await
        .expect("Failed to connect to session dbus");
    let nm = nm::NetworkManager::new(&connection)
        .await
        .expect("Failed to connect to NetworkManager dbus interface");

    loop {
        let netstate = get_active_connection_info(&nm).await;

        sender
            .send(ThreadMessage::JsonValue("nm".to_owned(), json!(netstate)))
            .unwrap();

        thread::sleep(std::time::Duration::from_secs(polling_s));
    }
    // let netstate = get_active_connection_info(&nm).await;
    // sender
    //     .send(ThreadMessage::JsonValue("nm".to_owned(), json!(netstate)))
    //     .unwrap();

    // match nm.receive_all_signals().await {
    //     Ok(signal_stream) => {
    //         signal_stream
    //             .for_each(|msg| async {
    //                 // println!("{:?}", msg.clone());
    //                 let netstate = get_active_connection_info(&nm).await;
    //                 sender
    //                     .send(ThreadMessage::JsonValue("nm".to_owned(), json!(netstate)))
    //                     .unwrap();
    //             })
    //             .await;
    //     }
    //     Err(_) => {}
    // }
}

impl MessageProducer for NetworkMonitoring {
    fn new(sender: mpsc::Sender<ThreadMessage>, config: &Config) -> Self {
        let polling_s = match config.get_int("nm.polling_s") {
            Ok(s) => s as u64,
            Err(_) => 60,
        };
        NetworkMonitoring { sender, polling_s }
    }

    fn run(&self) {
        let sender = self.sender.clone();
        let polling_s = self.polling_s;
        thread::spawn(move || {
            task::block_on(async {
                monitor(sender, polling_s).await;
            });
        });
    }
}

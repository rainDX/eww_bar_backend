/*
* SPDX-FileCopyrightText: Copyright © 2023 Romain Dijoux
* SPDX-License-Identifier: MIT
*/

use std::sync::mpsc;
use std::thread;

use config::Config;

use serde::Serialize;
use serde_json::json;

use crate::modules::{MessageProducer, ThreadMessage};

use async_std::task;
use upower_dbus::{DeviceProxy, UPowerProxy};
use zbus::{zvariant::OwnedObjectPath, Connection};


#[derive(Serialize)]
struct DeviceState {
    pub vendor: String,
    pub model: String,
    pub percentage: f64,
    pub charging: bool,
}

#[derive(Serialize)]
struct UPowerState {
    pub main_percentage: f64,
    pub has_battery: bool,
    pub charging: bool,
    pub devices: Vec<DeviceState>,
}

pub struct UPowerMonitoring {
    sender: mpsc::Sender<ThreadMessage>,
    polling_s: u64,
}

macro_rules! get_zbus_property {
    ($method:expr) => {
        match $method.await {
            Ok(prop) => Some(prop),
            Err(_) => None,
        }
    };
}

macro_rules! get_zbus_property_fallback {
    ($method:expr,$fallback:expr) => {
        match $method.await {
            Ok(prop) => prop,
            Err(_) => $fallback,
        }
    };
}

async fn get_device<'a>(
    upower: &'a UPowerProxy<'a>,
    path: &'a OwnedObjectPath,
) -> Option<DeviceProxy<'a>> {
    match DeviceProxy::builder(upower.connection()).path(path) {
        Ok(builder) => match builder.build().await {
            Ok(device) => Some(device),
            Err(_) => None,
        },
        Err(_) => None,
    }
}

async fn get_upower_info(upower: &UPowerProxy<'_>) -> UPowerState {
    let mut state = UPowerState {
        main_percentage: 0.0,
        has_battery: false,
        charging: false,
        devices: Vec::new(),
    };

    let devices = upower
        .enumerate_devices()
        .await
        .expect("Failed to get upower devices");

    for device in &devices {
        match get_device(upower, device).await {
            Some(device) => {
                let has_probably_battery = match get_zbus_property!(device.type_()) {
                    Some(bat_type) => match bat_type {
                        upower_dbus::BatteryType::Unknown
                        | upower_dbus::BatteryType::LinePower
                        | upower_dbus::BatteryType::Ups
                        | upower_dbus::BatteryType::Monitor => false,
                        upower_dbus::BatteryType::Battery
                        | upower_dbus::BatteryType::Mouse
                        | upower_dbus::BatteryType::Keyboard
                        | upower_dbus::BatteryType::Pda
                        | upower_dbus::BatteryType::Phone => {
                            get_zbus_property_fallback!(device.percentage(), 0.0) > 0.0
                        }
                    },
                    None => false,
                };
                if has_probably_battery {
                    let vendor = get_zbus_property_fallback!(device.vendor(), "".to_owned());
                    let model = get_zbus_property_fallback!(device.model(), "".to_owned());
                    let percentage = get_zbus_property_fallback!(device.percentage(), 0.0);
                    let charging = get_zbus_property_fallback!(
                        device.state(),
                        upower_dbus::BatteryState::Unknown
                    ) == upower_dbus::BatteryState::Charging;
                    let psu = get_zbus_property_fallback!(device.power_supply(), false);
                    if psu {
                        state.main_percentage = percentage;
                        state.has_battery = true;
                        state.charging = state.charging || charging;
                    }
                    state.devices.push(DeviceState {
                        vendor,
                        model,
                        percentage,
                        charging,
                    });
                }
            }
            None => {}
        }
    }
    state
}

async fn monitor(sender: mpsc::Sender<ThreadMessage>, polling_s: u64) {
    let connection = Connection::system()
        .await
        .expect("Failed to connect to session dbus");
    let upower = UPowerProxy::new(&connection)
        .await
        .expect("Failed to connect to UPower dbus interface");

    loop {
        let upower_state = get_upower_info(&upower).await;
        sender
            .send(ThreadMessage::JsonValue(
                "upower".to_owned(),
                json!(upower_state),
            ))
            .unwrap();
        thread::sleep(std::time::Duration::from_secs(polling_s));
    }
}

impl MessageProducer for UPowerMonitoring {
    fn new(sender: mpsc::Sender<ThreadMessage>, config: &Config) -> Self {
        let polling_s = match config.get_int("upower.polling_s") {
            Ok(s) => s as u64,
            Err(_) => 60,
        };
        UPowerMonitoring { sender, polling_s }
    }

    fn run(&self) {
        let sender = self.sender.clone();
        let polling_s = self.polling_s;
        thread::spawn(move || {
            task::block_on(async {
                monitor(sender, polling_s).await;
            });
        });
    }
}
